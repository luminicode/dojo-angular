module.exports = function(grunt) {
	grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    coffee: {
    	glob_to_multiple: {
    	    expand: true,
    	    cwd: 'src/main/coffee-src',
    	    src: ['**/*.coffee'],
    	    dest: 'src/main/webapp/js-dist',
    	    ext: '.js'
    	},
     },
     jasmine: {
		    pivotal: {
		      src: 'src/main/webapp/js-dist/spec/*.js',
		      options: {
            vendor: [
                "src/main/webapp/bower_components/angular/angular.js",
                "src/main/webapp/bower_components/angular-mocks/angular-mocks.js",
                "src/main/webapp/js-dist/*.js"
            ],
        	}
		    }
		 },
     watch: {
		  scripts: {
		    files: ['**/*.coffee'],
		    tasks: ['coffee', 'jasmine'],
		    options: {
		      spawn: false,
		    },
		  },
		},
	});

	grunt.loadNpmTasks('grunt-contrib-coffee');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jasmine');

	// Default task(s).
	grunt.registerTask('default', ['watch']);
}