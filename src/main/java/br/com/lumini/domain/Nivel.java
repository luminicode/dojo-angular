package br.com.lumini.domain;

public enum Nivel {

	ESTAGIARIO("ESTAGIARIO"), JUNIOR("JUNIOR"), PLENO("PLENO"), SENIOR("SENIOR"), ARQUITETO("ARQUITETO");

	private String value;

	Nivel(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

}