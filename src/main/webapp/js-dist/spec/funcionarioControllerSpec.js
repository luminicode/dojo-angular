(function() {
  describe("Funcionario", function() {
    beforeEach(module('app'));
    beforeEach(inject(function($rootScope, $controller, $httpBackend) {
      this.rootScope = $rootScope;
      this.controller = $controller;
      this.httpBackend = $httpBackend;
      this.scope = this.rootScope.$new();
      this.controller("Funcionario", {
        $scope: this.scope
      });
      return this.httpBackend.when("GET", /\/funcionario\/json.*/).respond(200, {
        funcionarios: [
          {
            "nome": "Pedro",
            "salario": 100,
            "telefone": "2015"
          }, {
            "nome": "Tiago",
            "salario": 1000,
            "telefone": "2016"
          }, {
            "nome": "Lucas",
            "salario": 10000,
            "telefone": "2017"
          }
        ]
      });
    }));
    return describe('Quando carregar lista de funcionarios', function() {
      it('deveriam ter 3 funcionarios', function() {
        this.scope.listaFuncionarios();
        this.httpBackend.flush();
        return expect(this.scope.funcionarios.length).toBe(3);
      });
      return it("Primeiro deve ter o nome de pedro", function() {
        this.scope.listaFuncionarios();
        this.httpBackend.flush();
        return expect(this.scope.funcionarios[0].nome).toBe('Pedro');
      });
    });
  });

}).call(this);
