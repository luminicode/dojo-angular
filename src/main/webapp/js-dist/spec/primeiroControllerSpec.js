(function() {
  describe("Primeiro", function() {
    beforeEach(module('app'));
    beforeEach(inject(function($rootScope, $controller) {
      this.rootScope = $rootScope;
      this.controller = $controller;
      this.scope = this.rootScope.$new();
      return this.controller("Primeiro", {
        $scope: this.scope
      });
    }));
    return describe('when loads the page', function() {
      return it('should fills a title variable', function() {
        return expect(this.scope.title).toBe('Título da página via angular!!!');
      });
    });
  });

}).call(this);
