(function() {
  angular.module('app').controller('Funcionario', function($scope, $http) {
    $scope.listaFuncionarios = function() {
      return $http.get("http://localhost:8080/dojo-angular/funcionario/json").success(function(data) {
        return $scope.funcionarios = data.funcionarios;
      });
    };
    return $scope.listaFuncionarios();
  });

}).call(this);
