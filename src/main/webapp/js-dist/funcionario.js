(function() {
  var Funcionario;

  Funcionario = (function() {
    function Funcionario(params) {
      this.nome = params.nome;
      this.telefone = params.telefone;
      this.salario = params.salario;
    }

    Funcionario.prototype.lista = function() {
      return $http.get("http://localhost:8080/dojo-angular/funcionario/json").success(function(data) {
        return $scope.funcionarios = data.funcionarios;
      });
    };

    Funcionario.prototype.salvar = function() {};

    Funcionario.prototype.deletar = function() {};

    return Funcionario;

  })();

}).call(this);
