<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="app">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<c:url value="/bower_components/bootstrap/dist/css/bootstrap.min.css"/>">
<script src="<c:url value="/bower_components/angular/angular.min.js"/>"></script>
<script src="<c:url value="/js-dist/app.js"/>"></script>
<script src="<c:url value="/js-dist/funcionarioController.js"/>"></script>
<title>Lista de Funcion�rios</title>
</head>
<body ng-controller="Funcionario">
	<div style="width:80%; margin:20px;">
		<h1>Lista de Funcion�rios</h1>
		<a href="${linkTo[FuncionarioController].form}" class="btn btn-success btn-default active" role="button">Novo Funcion�rio</a><br /><br />
		<table class="table table-striped">
			<thead>
				<th>Id</th>
				<th>Nome</th>
				<th>Telefone</th>
				<th>N�vel</th>
				<th>Sal�rio</th>
				<th>A��o</th>
			</thead>
			<tbody>
					<tr ng-repeat="funcionario in funcionarios">
						<td>{{funcionario.id}}</td>
						<td>{{funcionario.nome}}</td>
						<td>{{funcionario.telefone}}</td>
						<td>{{funcionario.nivel}}</td>
						<td>{{funcionario.salario}}</td>
						<td>
							<a href="form/{{funcionario.id}}" class="btn btn-info btn-default active" role="button">Editar Funcion�rio</a>
							<a href="deletar/{{funcionario.id}}" class="btn btn-danger btn-default active" role="button">Excluir Funcion�rio</a>
						</td>
					</tr>
			</tbody>
		</table>
	</div>
</body>
</html>