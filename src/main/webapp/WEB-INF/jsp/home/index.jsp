<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html ng-app="app">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Primeiro Controller</title>
<link rel="stylesheet" href="<c:url value="/bower_components/bootstrap/dist/css/bootstrap.min.css"/>">
<script src="<c:url value="/bower_components/angular/angular.min.js"/>"></script>
<script src="<c:url value="/js-dist/app.js"/>"></script>
<script src="<c:url value="/js-dist/primeiroController.js"/>"></script>
</head>
<body ng-controller="Primeiro">
	<h1>{{title}}</h1>
</body>
</html>