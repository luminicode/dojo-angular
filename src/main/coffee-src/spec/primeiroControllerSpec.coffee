describe "Primeiro", ->
  beforeEach module('app')
  beforeEach inject ($rootScope, $controller) ->
    @rootScope = $rootScope
    @controller = $controller
    @scope = @rootScope.$new()
    @controller "Primeiro", $scope: @scope
    
  describe 'when loads the page', -> 
    it 'should fills a title variable', ->
      expect(@scope.title).toBe 'Título da página via angular!!!'