describe "Funcionario", ->
  beforeEach module('app')
  beforeEach inject ($rootScope, $controller, $httpBackend) ->
    @rootScope = $rootScope
    @controller = $controller
    @httpBackend = $httpBackend
    @scope = @rootScope.$new()
    @controller "Funcionario", $scope: @scope
    @httpBackend.when("GET", ////funcionario/json.*///).respond 200,
      funcionarios: [
                  {"nome": "Pedro",
                  "salario": 100, 
                  "telefone": "2015"},
                  {"nome": "Tiago",
                  "salario": 1000, 
                  "telefone": "2016"},
                  {"nome": "Lucas",
                  "salario": 10000, 
                  "telefone": "2017"}                  
                ]
  describe 'Quando carregar lista de funcionarios', -> 
    it 'deveriam ter 3 funcionarios', ->
      @scope.listaFuncionarios()
      @httpBackend.flush()
      expect(@scope.funcionarios.length).toBe 3 
    
    it "Primeiro deve ter o nome de pedro", ->
      @scope.listaFuncionarios()
      @httpBackend.flush()
      expect(@scope.funcionarios[0].nome).toBe 'Pedro'


