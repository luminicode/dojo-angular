angular
  .module('app')
    .controller 'Funcionario', ($scope, $http) ->
      $scope.listaFuncionarios = ->
        $http.get("http://localhost:8080/dojo-angular/funcionario/json").success (data) ->
          $scope.funcionarios = data.funcionarios
      $scope.listaFuncionarios()