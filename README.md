#DOJO-ANGULAR #

Este é um projeto base para o dojo de angular. É basicamente um CRUD feito com vraptor 4

O projeto utiliza as seguintes tecnologias:

* Vraptor 4 com Maven
* Grunt
* Bower
* Bootstrap
* Angular

###Pré Requisitos###

* Java 7
* Tomcat 7
* Npm
* Eclipse com plugin do maven + plugin do tomcat

###Passos para importar o projeto###

* git clone git@bitbucket.org:luminicode/dojo-angular.git
* npm install (precisa do npm instalado ---> [http://nodejs.org/](http://nodejs.org/))
* abra o eclipse e importe o projeto como um maven-project

###Passos para rodar a parte java###

* adicione o server tomcat e rode o software pelo plugin do tomcat

###Passos para rodar a parte front###

* abra o terminal na pasta do projeto e digite grunt
* abra o seu editor preferido (recomendo sublime com plugin de coffee e angular)
* edite o código .coffee (fonte ou teste) e salve. Automaticamente os arquivos serão compilados para js e os testes serão rodados e os resultados aparecerão na janela do terminal aberta

###Problemas a serem resolvidos durante o DOJO###

* Modificar a listagem e o form(novo/editar) para o uso do angular.
* Fazer com que dado um salário consigamos inferir o nível do funcionário de acordo com essa tabela
>
  Descrição    |   Nível
  ------------------------    | ------
  Até R$ 1000,00              |   Estagiário
  De R$ 1001,00 a R$ 3500,00  |   Júnior
  De R$ 3501,00 a R$ 6000,00  |   Pleno
  De R$ 6001,00 a R$ 8500,00  |   Sênior
  Acima de R$ 8500,00         |   Arquiteto

* Desenvolver a lógica de promover um funcionário
* Desenvolver a lógica de rebaixar um funcionário